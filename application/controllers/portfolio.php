<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index($name=null)
	{
		$data = $this->data;
		$name = ($name) ? $name : 'occupywallstreet';

		if ($name)
		{
			$galleries = $this->config->item('galleries');

			if (!array_key_exists($name,$galleries))
			{
				die ("Not found");
			}

			$data['title'] = "Shameel Arafin Photography - ".$galleries[$name]['long_name'];
			$data['section'] = $galleries[$name];

			$data['gallery'] = $galleries[$name];
			$data['galleries'] = $galleries;

		}
		else
		{
			//

		}

		$data['name'] = $name;
		$this->layout->view('portfolio_view', $data);
	}
}
