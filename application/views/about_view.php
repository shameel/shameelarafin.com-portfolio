
	<div id="about">

		<div id="self_pic">
			<img src="<?=cdn_url()?>pub/img/shameel/shameel_medium_MG_2006.jpg" alt="" />
			<span>Photo: Maria April</span>
		</div>

		<div class="fb-like" data-href="http://shameelarafin.com/" data-send="false" data-width="450" data-show-faces="false" data-font="segoe ui"></div>

		<div id="statement">
			<p>
				<a href="<?=site_url('')?>">Shameel Arafin</a> (b.1975) is a
				freelance photographer based in New
				York City. Over the last two years, his photojournalism has
				revolved around the culture of resistance in America,
				specifically with regard to documenting the erosion of civil
				liberties.
			</p>

			<p>
				His photography has appeared in publications such as
				<em>CNN.com</em>,
				<em>The Sunday Times</em>,
				<em>The Daily Mail</em>,
				<em>Al Jazeera Fault Lines</em>,
				<em>The Progressive</em>,
				<em>Frieze Magazine</em>,
				<em>Art21</em>,
				<em>Socialist Worker</em>,
				<em>Kristeligt Dagblad</em>,
				<em>La Lettre de la Photographie</em>,
				<em>The Irish Times</em>,
				<em>Illume Magazine</em>,
				<em>Venture Beat</em>
				and
				<em>Make Magazine</em>.

			<p>
				Shameel studied literature at Harvard and electrical engineering
				at Caltech. He lives in	Brooklyn and works at <a target="_blank" href="http://mediastorm.com/">MediaStorm</a> as a software architect,
				where he is developing a <a target="_blank" href="http://mediastorm.com/pub/license/player">storytelling platform</a>
				for the distribution of cinematic, multimedia narratives. His
				work in multimedia software has been featured in <a target="_blank" href="http://lightbox.time.com/2012/06/12/game-changer-mediastorm-launches-pay-per-story-video-player/">Time Magazine's Lightbox</a>.
			</p>

			<p>
				Shameel is a member of <a target="_blank" href="http://www.nppa.org/profile/263099">NPPA</a>,
				and is available for editorial assignments. Select images are
				available on <a target="_blank" href="http://img.corbisimages.com/photographer/shameel-arafin">Corbis</a>.
			</p>
			
			<p><a href="<?=site_url('contact')?>">Contact Details</a></p>

		</div>
	</div>
