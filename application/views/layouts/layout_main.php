<!DOCTYPE html>
<html
	xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<title><?=(isset($title) ? $title : 'Shameel Arafin Photography')?></title>
		<meta charset="UTF-8" />
		<meta name="description" content="Shameel Arafin is a freelance photographer based in New York. Over the last two years, his photojournalism has revolved around the culture of resistance in America, specifically with regard to documenting the the erosion of civil liberties." />
		<meta name="keywords" content="Shameel Arafin, photojournalism, editorial, news, politics, Occupy Wall Street, photography, MediaStorm" />

		<meta property="og:type" content="website"/> 
		<meta property="og:site_name" content="Shameel Arafin Photography"/> 
		<meta property="og:url" content="<?=cdn_url()?>"/> 
		<meta property="og:title" content="Shameel Arafin Photography"/> 
		<meta property="og:description" content="Shameel Arafin is a freelance photographer based in New York. Visit http://shameelarafin.com/ for more information."/> 
		<meta property="og:image" content="<?=cdn_url()?>pub/galleries/occupywallstreet/images/occupy-8162.jpg"/> 
		<meta property="fb:admins" content="713071563" />
		<?//=$canonical?>

		<link rel="shortcut icon" href="<?=cdn_url()?>pub/img/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="<?=cdn_url()?>pub/css/portfolio.css" type="text/css" />

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-31699825-1']);
			_gaq.push(['_setDomainName', 'shameelarafin.com']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		
		<!--[if lt IE 7]>
			<script type="text/javascript" src="http://player.mediastorm.com/pub/js/unitpngfix.js"></script>
			<style type="text/css">
				.clearfix {height:1px;}
			</style>
		<![endif]-->

	</head>

	<body>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=169790016378458";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<div id="wrapper">
		
			<div id="header">
				<h1><a href="<?=site_url('')?>">Shameel Arafin Photography</a></h1>

				<div id="social_media">
					<a href="mailto:shameel@signalfive.com" title="Email">
						<img width="30px" src="<?=cdn_url()?>pub/img/inFocus_sidebar_social_media/Icons/grey/gmail.png" alt="Email" />
					</a>

					<a target="_blank" href="https://www.facebook.com/shameelarafinphoto" title="Facebook">
						<img width="30px" src="<?=cdn_url()?>pub/img/inFocus_sidebar_social_media/Icons/grey/facebook-logo.png" alt="Facebook" />
					</a>
					
					<a target="_blank" href="https://twitter.com/algebraist" title="Twitter">
						<img width="30px" src="<?=cdn_url()?>pub/img/inFocus_sidebar_social_media/Icons/grey/twitter-bird3.png" alt="Twitter" />
					</a>

					<a target="_blank" href="http://shameelarafin.tumblr.com" title="Tumblr">
						<img width="30px" src="<?=cdn_url()?>pub/img/inFocus_sidebar_social_media/Icons/grey/tumblr.png" alt="Tumblr" />
					</a>
				</div>
			</div>

			<?=$this->load->view('ui/panels/nav', null, true)?>

			<div id="content">
				<?=$content_for_layout?>

			</div>

			<div id="footer">
				All original work copyright &copy; 2010 - <?date_default_timezone_set('America/New_York'); echo date("Y") ?> 
				Shameel Arafin
			</div>

		</div>

	</body>

</html>
