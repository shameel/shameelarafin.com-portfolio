<?php
	$config['galleries'] = array(
		'thisoccupation' => array(
			'short_name'	=>	'occupy',
			'long_name'		=>	'This Occupation',
			'size'			=>	array('width'=> 800, 'height' => 640)
			),

		'capecod' => array(
			'short_name'	=>	'capecod',
			'long_name'		=>	'Cape Cod',
			'size'			=>	array('width'=> 800, 'height' => 640)
//			'gallery_text'	=>	'ui/panels/capecod_view'
			),

		'capecodiphone' => array(
			'short_name'	=>	'capecodiphone',
			'long_name'		=>	'Cape Cod iPhone',
			'size'			=>	array('width'=> 535, 'height' => 640)
			),


		'hurricanesandy' => array(
			'short_name'	=>	'hurricanesandy',
			'long_name'		=>	'Hurricane Sandy',
			'size'			=>	array('width'=> 800, 'height' => 640)
			),

		'singles' => array(
			'short_name'	=>	'singles',
			'long_name'		=>	'Singles',
			'size'			=>	array('width'=> 800, 'height' => 640)
			),

		'chicago1' => array(
			'short_name'	=>	'the-discovery-of-chicago-part-1',
			'long_name'		=>	'The Discovery of Chicago I',
			'size'			=>	array('width'=> 800, 'height' => 640)
			),


/*
		'portraits' => array(
			'short_name'	=>	'portraits',
			'long_name'		=>	'Portraits'
			),


/*		'rainbow-machine' => array(
			'short_name'	=>	'rainbow-machine',
			'long_name'		=>	'Rainbow Machine'
			),
*/

/*		'tearsheets' => array(
			'short_name'	=>	'tearsheets',
			'long_name'		=>	'Tearsheets'
			),
*/

		);


	$config['approved'] = array(
		'pub/galleries/occupywallstreet/images/occupy-6832.jpg',					// dragged shutter pointing finger
	);
