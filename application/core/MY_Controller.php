<?php
class MY_Controller extends CI_Controller {

	var $galleries = '';
	var $data;

	function __construct()
	{
		parent::__construct();

		$this->data['canonical'] = '<link rel="canonical" href="' . site_url( $this->uri->uri_string() ) .'" />';
	}
}