<?

	function show_xml($str)
	{
		echo ("<pre>".nl2br(htmlentities($str))."</pre>");

	}


	function vomit ($obj)
	{
		dump ($obj);
		die;
	}


	function dump($obj)
	{
		echo "<pre>";

		var_dump ($obj);

		echo "</pre>";

	}


	function html_to_content($html)
	{
		return html_entity_decode(strip_html_tags($html), ENT_QUOTES, "UTF-8");
	}


	function seo_loader()
	{
		// takes an array, and fills it with default SEO stuff
		$default_description = "SignalFive is a comprehensive interactive design and development company. We believe in principles of agile development practices, and the aesthetic that leaner is meaner. We believe in developing applications rapidly, and with a minimal resource footprint. We think, plan, design and develop in New York City.";
		$default_keywords = array(
			"web application development",
			"interactive design",
			"interactive agency",
			"new york web development",
			"motion graphics",
			"user experience",
			"user interaction"
		);
		$data['seo_description'] = $default_description;
		$data['seo_keywords'] = $default_keywords;

		return $data;
	}


	function seo_add_keywords($data, $keywords)
	{
		// takes the existing array and appends the new $keywords to it
		if (is_array($keywords))
		{
			return array_merge ($data, $keywords);
		}
		else
		{
			$data['seo_keywords'][] = $keywords;
			return $data;
		}
	}


	function seo_print_keywords($seo_keywords)
	{
		if ($seo_keywords)
		{
			foreach ($seo_keywords as $keyword)
			{
				echo $keyword.',';
			}
		}
	}

//	function seo_description($description=null)
//	{
//		$default = "SignalFive is a comprehensive interactive design and development company. We believe in principles of agile development practices, and the aesthetic that leaner is meaner. We believe in developing applications rapidly, and with a minimal resource footprint. We think, plan, design and develop in New York City.";
//
//		if ($description==null)
//			return $default;
//		else
//			return $description;
//
//
//	}

	if ( ! function_exists('cdn_url'))
	{
	    function cdn_url($uri = '')
	    {
	        $CI =& get_instance();
	        return $CI->config->item('cdn_url') .  $uri;
	        
	    }
	}
	

	/**
 * Remove HTML tags, including invisible text such as style and
 * script code, and embedded objects.  Add line breaks around
 * block-level tags to prevent word joining after tag removal.
 */
	function strip_html_tags( $text )
	{
	// from http://nadeausoftware.com/articles/2007/09/php_tip_how_strip_html_tags_web_page
	$text = preg_replace(
		array(
		// Remove invisible content
			'@<head[^>]*?>.*?</head>@siu',
			'@<style[^>]*?>.*?</style>@siu',
			'@<script[^>]*?.*?</script>@siu',
			'@<object[^>]*?.*?</object>@siu',
			'@<embed[^>]*?.*?</embed>@siu',
			'@<applet[^>]*?.*?</applet>@siu',
			'@<noframes[^>]*?.*?</noframes>@siu',
			'@<noscript[^>]*?.*?</noscript>@siu',
			'@<noembed[^>]*?.*?</noembed>@siu',
		// Add line breaks before and after blocks
			'@</?((address)|(blockquote)|(center)|(del))@iu',
			'@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
			'@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
			'@</?((table)|(th)|(td)|(caption))@iu',
			'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
			'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
			'@</?((frameset)|(frame)|(iframe))@iu',
		),
		array(
			' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
			"\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
			"\n\$0", "\n\$0",
		),
		$text );
		return strip_tags( $text );
	}


	function is_ajax()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
	}


	// check for valid url
	function is_valid_url ( $url )
	{
		$url = @parse_url($url);

		if ( ! $url) {
			return false;
		}

		$url = array_map('trim', $url);
		$url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
		$path = (isset($url['path'])) ? $url['path'] : '';

		if ($path == '')
		{
			$path = '/';
		}

		$path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';

		if ( isset ( $url['host'] ) AND $url['host'] != gethostbyname ( $url['host'] ) )
		{
			if ( PHP_VERSION >= 5 )
			{
				$headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
			}
			else
			{
				$fp = fsockopen($url['host'], $url['port'], $errno, $errstr, 30);

				if ( ! $fp )
				{
					return false;
				}
				fputs($fp, "HEAD $path HTTP/1.1\r\nHost: $url[host]\r\n\r\n");
				$headers = fread ( $fp, 128 );
				fclose ( $fp );
			}
			$headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
			return ( bool ) preg_match ( '#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers );
		}
		return false;
	}


	function limit_string($str, $length)
	{
		if (strlen($str)>$length)
		{
			return substr($str, 0, $length)."...";
		}
		else
		{
			return $str;
		}
	}


	function comma_list ($numbers)
	{
		if (is_array($numbers)) {
			// If numbers is an array then implode it into a comma separated string.
			$numbers = implode(',', $numbers);
		}

		if (is_string($numbers)) {
			/*
			Make sure all commas have a single space character after them and that
			there are no double commas in the string.
			*/
			$numbers = trim($numbers);
			$patterns[0] = '/\s*,\s*/';
			$patterns[1] = '/,{2,}/';
			$patterns[2] = '/,+$/';
			$patterns[3] = '/^,+/';
			$patterns[4] = '/,/';
			$replacements[0] = ',';
			$replacements[1] = ',';
			$replacements[2] = '';
			$replacements[3] = '';
			$replacements[4] = ', ';
			$numbers= preg_replace($patterns, $replacements, $numbers);

			// The string contains commas, find the last comma in the string.
			$lastCommaPos = strrpos($numbers, ',') - strlen($numbers);

			// Replace the last ocurrance of a comma with " and "
			$numbers = substr($numbers, 0, $lastCommaPos) . str_replace(',', ' and ', substr($numbers, $lastCommaPos));
		}
		return $numbers;
	}


	function strip_extension($strName)
	{
		$ext = strrchr($strName, '.');

		if($ext !== false)
		{
			$strName = substr($strName, 0, -strlen($ext));
		}
		return $strName;
	}


	function relative_date($timestamp)
	{
		// Get time difference and setup arrays
		$difference = time() - $timestamp;
		$periods = array("second", "minute", "hour", "day", "week", "month", "years");
		$lengths = array("60","60","24","7","4.35","12");

		// Past or present
		if ($difference >= 0)
		{
			$ending = "ago";
		}
		else
		{
			$difference = -$difference;
			$ending = "to go";
		}

		// Figure out difference by looping while less than array length
		// and difference is larger than lengths.
		$arr_len = count($lengths);
		for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++)
		{
			$difference /= $lengths[$j];
		}

		// Round up
		$difference = round($difference);

		// Make plural if needed
		if($difference != 1)
		{
			$periods[$j].= "s";
		}

		// Default format
		$text = "$difference $periods[$j] $ending";

		// over 24 hours
		if($j > 2)
		{
			// future date over a day formate with year
			if($ending == "to go")
			{
				if($j == 3 && $difference == 1)
				{
					$text = "Tomorrow at ". date("g:i a", $timestamp);
				}
				else
				{
					$text = date("F j, Y \a\\t g:i a", $timestamp);
				}
				return $text;
			}

			if($j == 3 && $difference == 1) // Yesterday
			{
				$text = "Yesterday at ". date("g:i a", $timestamp);
			}
			else if($j == 3) // Less than a week display -- Monday at 5:28pm
			{
				$text = date("l \a\\t g:i a", $timestamp);
			}
			else if($j < 6 && !($j == 5 && $difference == 12)) // Less than a year display -- June 25 at 5:23am
			{
				$text = date("F j \a\\t g:i a", $timestamp);
			}
			else // if over a year or the same month one year ago -- June 30, 2010 at 5:34pm
			{
				$text = date("F j, Y \a\\t g:i a", $timestamp);
			}
		}

		return $text;
	}


	?>
