
server {
	listen   80;
	server_name  blog.signalfive.com;
	root   /var/www/blog/;
	rewrite_log	on;

	access_log  /var/log/nginx/blog.signalfive.com.access.log;

	location /
	{
#		root   /var/www/blog.signalfive.com;
		index  index.php index.html index.htm ;

		if (!-e $request_filename) {
			rewrite ^/(.*)$ /index.php?q=$1 last;
		}
	}

	location ~ \.php$ {
			fastcgi_pass    127.0.0.1:9000;
			fastcgi_index   index.php;
			fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
			include         fastcgi_params;
	}
}



