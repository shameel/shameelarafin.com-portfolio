
server {
	listen   80;
	server_name  shameelarafin.com www.shameelarafin.com;
	root   /var/www/shameelarafin.com/www;
	rewrite_log	on;


#	auth_basic "Shameel Arafin Portfolio";
#	auth_basic_user_file /var/www/.htpasswd;

	access_log  /var/log/nginx/shameelarafin.com.access.log;

	location /
	{
		root   /var/www/shameelarafin.com/www;
		index  index.php index.html index.htm ;

		if (!-e $request_filename) {
			rewrite ^/(.*)$ /index.php?q=$1 last;
		}
	}

	location ~ \.php$ {
			fastcgi_pass    127.0.0.1:9000;
			fastcgi_index   index.php;
			fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
			include         fastcgi_params;
	}
}



